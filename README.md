# GRAIN: Truly Privacy-friendly and Self-Sovereign Trust Establishment with GNS and TRAIN

Robust and secure trust establishment is an open problem in the context of SSI. The
TRAIN concept proposes to leverage the security guarantees and trust anchor of the DNS to publish
and resolve pointers to trust lists from a secure directory. While the DNS is a corner stone of the
Internet, its continued use is primarily a consequence of inertia due to its crucial function as the
address discovery system for existing Internet services. Research and development in the area of SSI
is — for the most part — green field. The choice of DNS as a core building block appears fainthearted
given its open security issues. In this paper, we show how the GNU Name System [SGF23] as a
drop-in replacement for DNS in TRAIN [KR21] can deliver security and privacy improvements and
show that it is practically feasible with limited modifications of existing software stacks.

## Source Code
### TRAIN
https://gitlab.eclipse.org/eclipse/xfsc/train
### TRAIN components changed to use GNS
Train Trust Validator (/TCR): https://gitlab.eclipse.org/snadler/trusted-content-resolver/-/tree/gns?ref_type=heads

Train DNS Trustzone Manager (/ZoneManger): https://gitlab.eclipse.org/snadler/dns-zone-manager/-/tree/gns?ref_type=heads


### LIGHTest
https://github.com/H2020LIGHTest
### LIGHTest components changed to use GNS
Automatic Trust Verifier: https://github.com/Setrexis/AutomaticTrustVerifier

Trust Translation Authority: https://github.com/Setrexis/TrustTranslationAuthority

Trust Scheme Publication Authority: https://github.com/Setrexis/TrustSchemePublicationAuthority

ZoneManager: https://github.com/Setrexis/ZoneManager

Pumpkin Seed Oil Demo: https://github.com/Setrexis/PumpkinSeedOilDemo

## Bibliography
[SGF23] M. Schanzenbach, C. Grothoff, and B. Fix. The GNU Name System. RFC 9498. Nov. 2023. doi: 10.17487/RFC9498. url: https://www.rfc-editor.org/info/

[KR21]  M. Kubach and H. Roßnagel. “A lightweight trust management infrastructure for self-sovereign identity.” In: Open Identity Summit 2021 (2021).
